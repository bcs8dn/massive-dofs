#!/bin/bash
#mv ./*.eps ./
for f in ./*.eps; do epstopdf $f;  done
rm ./*.eps
for f in ./*.pdf; do mv $f ../pdf-files/;  done
git add ../pdf-files/*.pdf
git add ./*.agr
git pull
git commit -m "Updating plot pdf and agr"
git push
