(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     28911,        767]
NotebookOptionsPosition[     24523,        684]
NotebookOutlinePosition[     24888,        700]
CellTagsIndexPosition[     24845,        697]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Energy and r", "Subsubsection",
 CellChangeTimes->{{3.771768752224514*^9, 
  3.7717687542249393`*^9}},ExpressionUUID->"304fff83-0d5c-4edc-8ce4-\
0db8355ab059"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"En", "=", 
   RowBox[{
    FractionBox[
     RowBox[{"-", "1"}], "2"], "\[Mu]", " ", 
    SuperscriptBox["v", "2"]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"rval", " ", "=", " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      FractionBox[
       RowBox[{"\[ScriptCapitalG]", " ", "M"}], 
       RowBox[{"4", 
        SuperscriptBox["\[Pi]", "2"]}]], 
      SuperscriptBox["P", "2"]}], ")"}], 
    RowBox[{"1", "/", "3"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"vval", " ", "=", " ", 
   RowBox[{
    FractionBox[
     RowBox[{"2", "\[Pi]", " ", "rval"}], "P"], "//", "PowerExpand"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"Pval", " ", "=", " ", 
    RowBox[{"P", "/.", 
     RowBox[{
      RowBox[{"Solve", "[", 
       RowBox[{
        RowBox[{"%", "\[Equal]", "v"}], ",", "P"}], "]"}], "[", 
      RowBox[{"[", "1", "]"}], "]"}]}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"PdotGR", " ", "=", " ", 
   RowBox[{
    RowBox[{"ToExpression", "[", 
     RowBox[{
     "\"\<-\\\\frac{192 \\\\pi}{5} \\\\mu M^{2/3}\\\\left(\\\\frac{P}{2 \
\\\\pi}\\\\right)^{-\\\\frac{5}{3}}\>\"", ",", "TeXForm"}], "]"}], "/.", 
    RowBox[{"G", "\[Rule]", " ", "\[ScriptCapitalG]"}]}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.771768765029456*^9, 3.771768765210535*^9}, {
   3.771768897197424*^9, 3.771768927875279*^9}, {3.771769080612035*^9, 
   3.771769093709496*^9}, {3.7717693403173037`*^9, 3.771769396700111*^9}, {
   3.7717696885448103`*^9, 3.7717697118388643`*^9}, {3.771769794756407*^9, 
   3.771769795100868*^9}, 3.7717698268883953`*^9},
 CellLabel->
  "In[400]:=",ExpressionUUID->"fef461f3-14ed-4a63-9c78-db1663101226"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Edot expressions", "Subsubsection",
 CellChangeTimes->{{3.771768760725852*^9, 
  3.7717687732529087`*^9}},ExpressionUUID->"f980c88b-d321-402a-a093-\
438128686e92"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"EndotGRModQuad", "=", 
    RowBox[{
     RowBox[{"ToExpression", "[", 
      RowBox[{
      "\"\<-\\\\frac{32}{5}\\\\frac{\\\\mathcal{G}^{2}\\\\mu^{2} M^{2} \
v^{2}}{r^{4}}\\\\left(1-\\\\frac{1}{2}\\\\xi\\\\right)\>\"", ",", "TeXForm"}],
       "]"}], "/.", 
     RowBox[{"G", "\[Rule]", " ", "\[ScriptCapitalG]"}]}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"EndotScQuad", "=", 
   RowBox[{
    RowBox[{"ToExpression", "[", 
     RowBox[{
     "\"\<-\\\\frac{\\\\mathcal{G}^{2} \\\\mu^{2} M^{2} \
\\\\xi}{r^{4}}\\\\frac{8}{15} \\\\Gamma^{2} v^{2}\\\\left(\\\\frac{4 \
\\\\omega^{2}-m_{s}^{2}}{4 \\\\omega^{2}}\\\\right)^{2} \\\\Theta\\\\left(2 \
\\\\omega-m_{s} \\\\right)\>\"", ",", "TeXForm"}], "]"}], "/.", 
    RowBox[{"G", "\[Rule]", " ", "\[ScriptCapitalG]"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"EndotQuad", "=", 
   RowBox[{"EndotGRModQuad", "+", "EndotScQuad"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"EndotScDip", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"ToExpression", "[", 
     RowBox[{
     "\"\<-\\\\frac{\\\\mathcal{G}^{2} \\\\mu^{2} M^{2} \
\\\\xi}{r^{4}}\\\\frac{2}{3} \\\\mathcal{S}^{2} \
\\\\frac{\\\\omega^{2}-m_{\\\\mathrm{s}}^{2}}{\\\\omega^{2}}\\\\Theta\\\\left(\
\\\\omega-m_{s}\\\\right)\>\"", ",", "TeXForm"}], "]"}], "/.", 
    RowBox[{"G", "\[Rule]", " ", "\[ScriptCapitalG]"}]}], "/.", 
   RowBox[{"S", "\[Rule]", " ", "\[ScriptCapitalS]"}]}]}]}], "Input",
 CellChangeTimes->{{3.771766470962823*^9, 3.7717666012530317`*^9}, {
   3.771766676563052*^9, 3.7717668430749073`*^9}, {3.771766876876055*^9, 
   3.771766901928124*^9}, {3.771766997179694*^9, 3.771767014874814*^9}, {
   3.771767097094892*^9, 3.771767106625493*^9}, {3.771767155013785*^9, 
   3.771767156227212*^9}, {3.7717674291111603`*^9, 3.771767516887497*^9}, {
   3.771767586521283*^9, 3.771767665964632*^9}, {3.771767838092515*^9, 
   3.771767852898383*^9}, {3.771767972031333*^9, 3.7717679954131603`*^9}, {
   3.771768111719954*^9, 3.771768175500125*^9}, {3.771768488505822*^9, 
   3.771768509217196*^9}, {3.77176859575716*^9, 3.7717686077071123`*^9}, {
   3.7717687060341797`*^9, 3.771768779469507*^9}, {3.771770093423904*^9, 
   3.77177011689172*^9}, {3.7717701707878227`*^9, 3.7717702031265306`*^9}, 
   3.771770587500218*^9},
 CellLabel->
  "In[405]:=",ExpressionUUID->"27ce4f4b-d8bb-462d-8cc1-221333f8328a"],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"2", " ", 
    SuperscriptBox["M", "2"], " ", 
    SuperscriptBox["\[ScriptCapitalG]", "2"], " ", 
    SuperscriptBox["\[ScriptCapitalS]", "2"], " ", 
    SuperscriptBox["\[Mu]", "2"], " ", "\[Xi]", " ", 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox["\[Omega]", "2"], "-", 
      SubsuperscriptBox["m", "s", "2"]}], ")"}], " ", 
    RowBox[{"\[CapitalTheta]", "[", 
     RowBox[{"\[Omega]", "-", 
      SubscriptBox["m", "s"]}], "]"}]}], 
   RowBox[{"3", " ", 
    SuperscriptBox["r", "4"], " ", 
    SuperscriptBox["\[Omega]", "2"]}]]}]], "Output",
 CellChangeTimes->{3.771770587986786*^9, 3.7717714176104403`*^9},
 CellLabel->
  "Out[408]=",ExpressionUUID->"c9cd9b0f-bb77-4a6b-8cbc-09236cca7b38"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\[Kappa] values", "Subsubsection",
 CellChangeTimes->{{3.77176872765646*^9, 
  3.7717687347835083`*^9}},ExpressionUUID->"bed51261-985b-4067-bf67-\
e983bcbd1518"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"kappa1", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"ToExpression", "[", 
     RowBox[{
     "\"\<G^{2}*(12-6 \\\\xi+\\\\xi* \\\\Gamma^{2}\\\\left(\\\\frac{4 \
\\\\omega^{2}-m_{s}^{2}}{4 \\\\omega^{2}}\\\\right)^{2} \\\\Theta\\\\left(2 \
\\\\omega-m_{s}\\\\right))\>\"", ",", "TeXForm"}], "]"}], "/.", 
    RowBox[{"G", "\[Rule]", " ", "\[ScriptCapitalG]"}]}], "//", 
   "Distribute"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"kappaD", "=", 
   RowBox[{
    RowBox[{"ToExpression", "[", 
     RowBox[{
     "\"\<2 G \\\\xi \
\\\\frac{\\\\omega^{2}-m_{s}^{2}}{\\\\omega^{2}}\\\\Theta\\\\left(\\\\omega-m_\
{s}\\\\right)\>\"", ",", "TeXForm"}], "]"}], "/.", 
    RowBox[{"G", "\[Rule]", " ", "\[ScriptCapitalG]"}]}]}], ";"}]}], "Input",
 CellChangeTimes->{
  3.7717688658005543`*^9, 3.7717700840182333`*^9, {3.7717702717487717`*^9, 
   3.77177036253027*^9}, {3.771770407094554*^9, 3.771770414750085*^9}},
 CellLabel->
  "In[409]:=",ExpressionUUID->"c603d323-5633-4ec5-9e6f-f07d49a08bac"],

Cell[BoxData[
 RowBox[{
  RowBox[{"12", " ", 
   SuperscriptBox["\[ScriptCapitalG]", "2"]}], "-", 
  RowBox[{"6", " ", 
   SuperscriptBox["\[ScriptCapitalG]", "2"], " ", "\[Xi]"}], "+", 
  FractionBox[
   RowBox[{
    SuperscriptBox["\[ScriptCapitalG]", "2"], " ", 
    SuperscriptBox["\[CapitalGamma]", "2"], " ", "\[Xi]", " ", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"4", " ", 
        SuperscriptBox["\[Omega]", "2"]}], "-", 
       SubsuperscriptBox["m", "s", "2"]}], ")"}], "2"], " ", 
    RowBox[{"\[CapitalTheta]", "[", 
     RowBox[{
      RowBox[{"2", " ", "\[Omega]"}], "-", 
      SubscriptBox["m", "s"]}], "]"}]}], 
   RowBox[{"16", " ", 
    SuperscriptBox["\[Omega]", "4"]}]]}]], "Output",
 CellChangeTimes->{{3.7717704074598713`*^9, 3.771770436232997*^9}, 
   3.771771417634901*^9},
 CellLabel->
  "Out[409]=",ExpressionUUID->"cd9b82e9-2c05-400e-8428-a254c4397eb8"]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["Alsing check values", "Subsubsection",
 CellChangeTimes->{{3.7717687134863443`*^9, 
  3.7717687171857033`*^9}},ExpressionUUID->"c7f60019-cba3-4fd1-a8c7-\
d4cfe96645c3"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"PdotoverPscDAlsing", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"ToExpression", "[", 
     RowBox[{
     "\"\<\\\\frac{5}{96} M^{-(2 / 3)}\\\\left(\\\\frac{2 \
\\\\pi}{P}\\\\right)^{-2 / 3} 2 \\\\mathcal{S}^{2} \\\\kappa_{D}\>\"", ",", 
      "TeXForm"}], "]"}], "/.", 
    RowBox[{"S", "\[Rule]", " ", "\[ScriptCapitalS]"}]}], "//", 
   "PowerExpand"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"PdotoverPQuadAlsing", "=", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"ToExpression", "[", 
       RowBox[{"\"\< G^{-4/3}\\\\kappa_{1}/12\>\"", ",", "TeXForm"}], "]"}], "/.", 
      RowBox[{"S", "\[Rule]", " ", "\[ScriptCapitalS]"}]}], "/.", 
     RowBox[{"G", "\[Rule]", " ", "\[ScriptCapitalG]"}]}], "//", 
    "PowerExpand"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.7717700290744658`*^9, 3.771770068730607*^9}, 
   3.771770483456293*^9},
 CellLabel->
  "In[411]:=",ExpressionUUID->"ff4abe39-f9a0-4c72-b5f0-1e4cecaaa546"],

Cell[BoxData[
 FractionBox[
  RowBox[{"5", " ", 
   SuperscriptBox["P", 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox["\[ScriptCapitalS]", "2"], " ", 
   SubscriptBox["\[Kappa]", "D"]}], 
  RowBox[{"48", " ", 
   SuperscriptBox["M", 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"2", " ", "\[Pi]"}], ")"}], 
    RowBox[{"2", "/", "3"}]]}]]], "Output",
 CellChangeTimes->{
  3.771768719357073*^9, {3.7717700659401627`*^9, 3.771770069172224*^9}, 
   3.771770484226317*^9, 3.771771417669186*^9},
 CellLabel->
  "Out[411]=",ExpressionUUID->"68240a31-c500-4ccc-b5ca-94cc6a5894a7"]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["Dipole check", "Subsection",
 CellChangeTimes->{{3.7717679430494967`*^9, 
  3.771767945094532*^9}},ExpressionUUID->"ea61d37a-3fd4-4a1a-8918-\
8282d9e56eee"],

Cell["Pdot/P equals:", "Text",
 CellChangeTimes->{{3.771768268282653*^9, 
  3.7717682722717533`*^9}},ExpressionUUID->"88741a4d-f78d-4798-95d2-\
1af57d4d42be"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"-", 
     FractionBox["3", "2"]}], 
    FractionBox["EndotScDip", 
     RowBox[{" ", "En"}]], 
    RowBox[{"(", 
     FractionBox["P", 
      RowBox[{"PdotGR", " "}]], ")"}]}], "/.", 
   RowBox[{"r", "\[Rule]", " ", "rval"}]}], "//", 
  "PowerExpand"}], "\[IndentingNewLine]", 
 RowBox[{"fracPdotDip", "=", 
  RowBox[{
   RowBox[{
    FractionBox["%", "kappaD"], 
    SubscriptBox["\[Kappa]", "D"]}], "//", "PowerExpand"}]}]}], "Input",
 CellChangeTimes->{{3.771767948939734*^9, 3.771767950667362*^9}, {
  3.771768002646392*^9, 3.771768007149425*^9}, {3.771768043975477*^9, 
  3.7717680850826693`*^9}, {3.771768144153264*^9, 3.771768146501532*^9}, {
  3.771768227593382*^9, 3.77176823906596*^9}, {3.771770498685245*^9, 
  3.771770499919795*^9}, {3.771771282906508*^9, 3.771771286631288*^9}},
 CellLabel->
  "In[428]:=",ExpressionUUID->"4c3cae32-8d9f-4405-bc20-768b747b5615"],

Cell[BoxData[
 FractionBox[
  RowBox[{"5", " ", 
   SuperscriptBox["\[ScriptCapitalG]", 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox["\[ScriptCapitalS]", "2"], " ", "\[Xi]", " ", 
   RowBox[{"(", 
    RowBox[{
     SuperscriptBox["\[Omega]", "2"], "-", 
     SubsuperscriptBox["m", "s", "2"]}], ")"}], " ", 
   RowBox[{"\[CapitalTheta]", "[", 
    RowBox[{"\[Omega]", "-", 
     SubscriptBox["m", "s"]}], "]"}]}], 
  RowBox[{"48", " ", 
   SuperscriptBox["v", "2"], " ", 
   SuperscriptBox["\[Omega]", "2"]}]]], "Output",
 CellChangeTimes->{
  3.7717679509846277`*^9, {3.771767999527439*^9, 3.771768007455369*^9}, {
   3.77176804451612*^9, 3.771768085366425*^9}, {3.7717681382232447`*^9, 
   3.7717681469625587`*^9}, 3.771768179205349*^9, 3.771768230855053*^9, {
   3.771768379415584*^9, 3.7717684076455393`*^9}, {3.7717685145431557`*^9, 
   3.771768565957629*^9}, 3.7717698036137877`*^9, 3.7717705024823027`*^9, 
   3.771771288131289*^9, 3.771771417694528*^9, 3.771771927712082*^9},
 CellLabel->
  "Out[428]=",ExpressionUUID->"f7764a79-760e-4eaf-9d14-59e504d65a21"],

Cell[BoxData[
 FractionBox[
  RowBox[{"5", " ", 
   SuperscriptBox["\[ScriptCapitalS]", "2"], " ", 
   SubscriptBox["\[Kappa]", "D"]}], 
  RowBox[{"96", " ", 
   SuperscriptBox["v", "2"], " ", 
   SuperscriptBox["\[ScriptCapitalG]", 
    RowBox[{"1", "/", "3"}]]}]]], "Output",
 CellChangeTimes->{
  3.7717679509846277`*^9, {3.771767999527439*^9, 3.771768007455369*^9}, {
   3.77176804451612*^9, 3.771768085366425*^9}, {3.7717681382232447`*^9, 
   3.7717681469625587`*^9}, 3.771768179205349*^9, 3.771768230855053*^9, {
   3.771768379415584*^9, 3.7717684076455393`*^9}, {3.7717685145431557`*^9, 
   3.771768565957629*^9}, 3.7717698036137877`*^9, 3.7717705024823027`*^9, 
   3.771771288131289*^9, 3.771771417694528*^9, 3.7717719277135572`*^9},
 CellLabel->
  "Out[429]=",ExpressionUUID->"b443ccae-6af1-49ab-b219-028e14780631"]
}, Open  ]],

Cell["Difference from Alsing:", "Text",
 CellChangeTimes->{{3.77176824226336*^9, 3.771768246819881*^9}, {
  3.771768300425947*^9, 
  3.7717683029111*^9}},ExpressionUUID->"009d1802-d61c-4725-bea0-4f04ef21a0fa"],

Cell[CellGroupData[{

Cell[BoxData[{
 FractionBox["%", "PdotoverPscDAlsing"], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"%", "/.", 
   RowBox[{"v", "\[Rule]", " ", 
    FractionBox[
     RowBox[{"2", "\[Pi]", " ", "rval"}], "P"]}]}], "//", 
  "PowerExpand"}]}], "Input",
 CellChangeTimes->{{3.7717683041683064`*^9, 3.771768370205366*^9}, {
   3.771768414239049*^9, 3.7717684620328913`*^9}, {3.771768524348919*^9, 
   3.771768562626379*^9}, 3.771771854690707*^9},
 CellLabel->
  "In[430]:=",ExpressionUUID->"bc1d5add-0e4b-4ffd-85df-867cb4c59e65"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["M", 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox["\[Pi]", 
    RowBox[{"2", "/", "3"}]]}], 
  RowBox[{
   SuperscriptBox["2", 
    RowBox[{"1", "/", "3"}]], " ", 
   SuperscriptBox["P", 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox["v", "2"], " ", 
   SuperscriptBox["\[ScriptCapitalG]", 
    RowBox[{"1", "/", "3"}]]}]]], "Output",
 CellChangeTimes->{{3.771768381339458*^9, 3.771768407685588*^9}, {
   3.771768438143126*^9, 3.771768462322735*^9}, {3.771768514594987*^9, 
   3.771768565983971*^9}, 3.7717698036476307`*^9, 3.7717705025219812`*^9, 
   3.771771417717847*^9, 3.771771855842828*^9, 3.771771927762776*^9},
 CellLabel->
  "Out[430]=",ExpressionUUID->"5ddbff22-4c8c-461a-bbe2-392af68ecd57"],

Cell[BoxData[
 FractionBox["1", 
  RowBox[{"2", " ", "\[ScriptCapitalG]"}]]], "Output",
 CellChangeTimes->{{3.771768381339458*^9, 3.771768407685588*^9}, {
   3.771768438143126*^9, 3.771768462322735*^9}, {3.771768514594987*^9, 
   3.771768565983971*^9}, 3.7717698036476307`*^9, 3.7717705025219812`*^9, 
   3.771771417717847*^9, 3.771771855842828*^9, 3.77177192776442*^9},
 CellLabel->
  "Out[431]=",ExpressionUUID->"2b020099-8d88-477a-88b9-b5f77ecb585a"]
}, Open  ]],

Cell["What Alsing should have had:", "Text",
 CellChangeTimes->{{3.7717718635514*^9, 
  3.771771868970295*^9}},ExpressionUUID->"aff69f27-c245-4ef4-a381-\
7c050a6c7051"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"fracPdotDip", "/.", 
  RowBox[{"v", "\[Rule]", " ", "vval"}]}]], "Input",
 CellChangeTimes->{{3.771771878888715*^9, 3.771771881904373*^9}},
 CellLabel->
  "In[432]:=",ExpressionUUID->"fd59c4cc-db8d-4189-a4f7-6d0f4361d7d9"],

Cell[BoxData[
 FractionBox[
  RowBox[{"5", " ", 
   SuperscriptBox["P", 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox["\[ScriptCapitalS]", "2"], " ", 
   SubscriptBox["\[Kappa]", "D"]}], 
  RowBox[{"96", " ", 
   SuperscriptBox["M", 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"2", " ", "\[Pi]"}], ")"}], 
    RowBox[{"2", "/", "3"}]], " ", "\[ScriptCapitalG]"}]]], "Output",
 CellChangeTimes->{3.771771882204423*^9, 3.7717719277885113`*^9},
 CellLabel->
  "Out[432]=",ExpressionUUID->"aa9b2292-d177-4cc8-8a5d-a13eb5fc000d"]
}, Open  ]],

Cell["Gamma value for -1PN", "Text",
 CellChangeTimes->{{3.771771295641296*^9, 3.7717713039990597`*^9}, {
  3.771771375867546*^9, 
  3.7717713779023027`*^9}},ExpressionUUID->"ccc2b872-c740-4efc-ba75-\
95c308286ec1"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  FractionBox["fracPdotDip", 
   SuperscriptBox["v", 
    RowBox[{"-", "2"}]]], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Framed", "[", 
  RowBox[{
   SubscriptBox["\[Gamma]", 
    RowBox[{"-", "1"}]], "\[Equal]", "%"}], "]"}]}], "Input",
 CellChangeTimes->{{3.771771306754428*^9, 3.7717713621548758`*^9}, {
  3.77177141405867*^9, 3.771771414689102*^9}},
 CellLabel->
  "In[433]:=",ExpressionUUID->"f63d9340-a163-4b06-806d-d70b035e1b94"],

Cell[BoxData[
 FrameBox[
  RowBox[{
   SubscriptBox["\[Gamma]", 
    RowBox[{"-", "1"}]], "\[Equal]", 
   FractionBox[
    RowBox[{"5", " ", 
     SuperscriptBox["\[ScriptCapitalS]", "2"], " ", 
     SubscriptBox["\[Kappa]", "D"]}], 
    RowBox[{"96", " ", 
     SuperscriptBox["\[ScriptCapitalG]", 
      RowBox[{"1", "/", "3"}]]}]]}],
  StripOnInput->False]], "Output",
 CellChangeTimes->{3.771771316282013*^9, 3.771771362606216*^9, 
  3.771771417745129*^9, 3.7717719278124638`*^9},
 CellLabel->
  "Out[434]=",ExpressionUUID->"9767ab1a-0049-42ae-9f8e-ad322a5f882e"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Quad check", "Subsection",
 CellChangeTimes->{{3.7717679430494967`*^9, 3.771767945094532*^9}, {
  3.771770080429356*^9, 
  3.771770080905464*^9}},ExpressionUUID->"1a86bdb0-d846-4061-976e-\
29c9f0e6efb2"],

Cell["Pdot/P equals:", "Text",
 CellChangeTimes->{{3.771768268282653*^9, 
  3.7717682722717533`*^9}},ExpressionUUID->"67816b1d-6c76-462c-9de4-\
40f42f470f41"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"-", 
      FractionBox["3", "2"]}], 
     FractionBox["EndotQuad", 
      RowBox[{" ", "En"}]], 
     FractionBox["P", 
      RowBox[{"PdotGR", " "}]]}], "/.", 
    RowBox[{"r", "\[Rule]", " ", "rval"}]}], "//", "PowerExpand"}], "//", 
  "Simplify"}], "\[IndentingNewLine]", 
 RowBox[{"fracPdotQuad", "=", 
  RowBox[{
   RowBox[{
    RowBox[{
     FractionBox["%", "kappa1"], 
     SubscriptBox["\[Kappa]", "1"]}], "//", "PowerExpand"}], "//", 
   "Simplify"}]}]}], "Input",
 CellChangeTimes->{{3.771767948939734*^9, 3.771767950667362*^9}, {
   3.771768002646392*^9, 3.771768007149425*^9}, {3.771768043975477*^9, 
   3.7717680850826693`*^9}, {3.771768144153264*^9, 3.771768146501532*^9}, {
   3.771768227593382*^9, 3.77176823906596*^9}, {3.7717701238211927`*^9, 
   3.771770159496904*^9}, {3.771770214700526*^9, 3.7717702255553837`*^9}, {
   3.771770256934456*^9, 3.771770258149119*^9}, {3.7717703747432613`*^9, 
   3.77177038071268*^9}, {3.771770492421713*^9, 3.771770494409639*^9}, 
   3.771771108270154*^9, {3.771771396534917*^9, 3.771771400536083*^9}},
 CellLabel->
  "In[419]:=",ExpressionUUID->"5dd7ad58-395c-493f-9dbc-81bfbb18c348"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["\[ScriptCapitalG]", 
    RowBox[{"2", "/", "3"}]], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "96"}], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "2"}], "+", "\[Xi]"}], ")"}], " ", 
      SuperscriptBox["\[Omega]", "4"]}], "+", 
     RowBox[{
      SuperscriptBox["\[CapitalGamma]", "2"], " ", "\[Xi]", " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          RowBox[{"-", "4"}], " ", 
          SuperscriptBox["\[Omega]", "2"]}], "+", 
         SubsuperscriptBox["m", "s", "2"]}], ")"}], "2"], " ", 
      RowBox[{"\[CapitalTheta]", "[", 
       RowBox[{
        RowBox[{"2", " ", "\[Omega]"}], "-", 
        SubscriptBox["m", "s"]}], "]"}]}]}], ")"}]}], 
  RowBox[{"192", " ", 
   SuperscriptBox["\[Omega]", "4"]}]]], "Output",
 CellChangeTimes->{
  3.7717679509846277`*^9, {3.771767999527439*^9, 3.771768007455369*^9}, {
   3.77176804451612*^9, 3.771768085366425*^9}, {3.7717681382232447`*^9, 
   3.7717681469625587`*^9}, 3.771768179205349*^9, 3.771768230855053*^9, {
   3.771768379415584*^9, 3.7717684076455393`*^9}, {3.7717685145431557`*^9, 
   3.771768565957629*^9}, 3.7717698036137877`*^9, {3.771770125587019*^9, 
   3.771770163470561*^9}, {3.771770206277788*^9, 3.7717702259062433`*^9}, 
   3.771770258457136*^9, {3.771770366482904*^9, 3.7717703810522757`*^9}, 
   3.771770419081098*^9, {3.7717704672523193`*^9, 3.771770505326867*^9}, 
   3.7717711267897587`*^9, {3.771771400883933*^9, 3.7717714177728024`*^9}},
 CellLabel->
  "Out[419]=",ExpressionUUID->"9e10b7fa-14f8-43e3-bfef-12125df5242f"],

Cell[BoxData[
 FractionBox[
  SubscriptBox["\[Kappa]", "1"], 
  RowBox[{"12", " ", 
   SuperscriptBox["\[ScriptCapitalG]", 
    RowBox[{"4", "/", "3"}]]}]]], "Output",
 CellChangeTimes->{
  3.7717679509846277`*^9, {3.771767999527439*^9, 3.771768007455369*^9}, {
   3.77176804451612*^9, 3.771768085366425*^9}, {3.7717681382232447`*^9, 
   3.7717681469625587`*^9}, 3.771768179205349*^9, 3.771768230855053*^9, {
   3.771768379415584*^9, 3.7717684076455393`*^9}, {3.7717685145431557`*^9, 
   3.771768565957629*^9}, 3.7717698036137877`*^9, {3.771770125587019*^9, 
   3.771770163470561*^9}, {3.771770206277788*^9, 3.7717702259062433`*^9}, 
   3.771770258457136*^9, {3.771770366482904*^9, 3.7717703810522757`*^9}, 
   3.771770419081098*^9, {3.7717704672523193`*^9, 3.771770505326867*^9}, 
   3.7717711267897587`*^9, {3.771771400883933*^9, 3.7717714177745247`*^9}},
 CellLabel->
  "Out[420]=",ExpressionUUID->"48dbfab1-858b-4a51-a6f8-9ce24cb52701"]
}, Open  ]],

Cell["Difference from Alsing:", "Text",
 CellChangeTimes->{{3.77176824226336*^9, 3.771768246819881*^9}, {
  3.771768300425947*^9, 
  3.7717683029111*^9}},ExpressionUUID->"9995f71f-1a71-43b0-8c62-d4adcd24561d"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   FractionBox["%", "PdotoverPQuadAlsing"], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"%", "/.", 
     RowBox[{"v", "\[Rule]", " ", 
      FractionBox[
       RowBox[{"2", "\[Pi]", " ", "rval"}], "P"]}]}], "//", 
    "PowerExpand"}]}]}]], "Input",
 CellChangeTimes->{{3.7717683041683064`*^9, 3.771768370205366*^9}, {
  3.771768414239049*^9, 3.7717684620328913`*^9}, {3.771768524348919*^9, 
  3.771768562626379*^9}, {3.771770447507184*^9, 3.771770453890774*^9}},
 CellLabel->
  "In[421]:=",ExpressionUUID->"32c8278a-67fa-41d9-8a14-0bf9b46ad126"],

Cell[BoxData["1"], "Output",
 CellChangeTimes->{{3.771768381339458*^9, 3.771768407685588*^9}, {
   3.771768438143126*^9, 3.771768462322735*^9}, {3.771768514594987*^9, 
   3.771768565983971*^9}, 3.7717698036476307`*^9, 3.771770419111945*^9, {
   3.771770454232876*^9, 3.771770505359446*^9}, 3.7717714178020573`*^9},
 CellLabel->
  "Out[421]=",ExpressionUUID->"8d4a0082-5ff4-430a-aaa4-463657ba541a"],

Cell[BoxData["1"], "Output",
 CellChangeTimes->{{3.771768381339458*^9, 3.771768407685588*^9}, {
   3.771768438143126*^9, 3.771768462322735*^9}, {3.771768514594987*^9, 
   3.771768565983971*^9}, 3.7717698036476307`*^9, 3.771770419111945*^9, {
   3.771770454232876*^9, 3.771770505359446*^9}, 3.771771417803589*^9},
 CellLabel->
  "Out[422]=",ExpressionUUID->"b486762a-9e52-41dc-ba66-aaeab527b5de"]
}, Open  ]],

Cell["Gamma value for 0PN", "Text",
 CellChangeTimes->{{3.771771295641296*^9, 
  3.7717713039990597`*^9}},ExpressionUUID->"e34773bd-4aea-464a-b939-\
b102a0e6e444"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  FractionBox["fracPdotQuad", 
   SuperscriptBox["v", 
    RowBox[{"-", "0"}]]], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Framed", "[", 
  RowBox[{
   SubscriptBox["\[Gamma]", "0"], "\[Equal]", "%"}], "]"}]}], "Input",
 CellChangeTimes->{{3.771771306754428*^9, 3.7717713621548758`*^9}, {
  3.7717714053251667`*^9, 3.771771407841991*^9}},
 CellLabel->
  "In[423]:=",ExpressionUUID->"9b6c60d0-3f87-43cc-bfe4-bd10013a9a91"],

Cell[BoxData[
 FrameBox[
  RowBox[{
   SubscriptBox["\[Gamma]", "0"], "\[Equal]", 
   FractionBox[
    SubscriptBox["\[Kappa]", "1"], 
    RowBox[{"12", " ", 
     SuperscriptBox["\[ScriptCapitalG]", 
      RowBox[{"4", "/", "3"}]]}]]}],
  StripOnInput->False]], "Output",
 CellChangeTimes->{3.771771316282013*^9, 3.771771362606216*^9, 
  3.77177141783184*^9},
 CellLabel->
  "Out[424]=",ExpressionUUID->"5b974c98-b98c-4fb4-91b8-fc56864f84bd"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{808, 911},
WindowMargins->{{-954, Automatic}, {0, Automatic}},
Magnification:>0.9 Inherited,
FrontEndVersion->"12.0 for Linux x86 (64-bit) (April 8, 2019)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 165, 3, 41, "Subsubsection",ExpressionUUID->"304fff83-0d5c-4edc-8ce4-0db8355ab059"],
Cell[748, 27, 1759, 48, 265, "Input",ExpressionUUID->"fef461f3-14ed-4a63-9c78-db1663101226"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2544, 80, 169, 3, 41, "Subsubsection",ExpressionUUID->"f980c88b-d321-402a-a093-438128686e92"],
Cell[CellGroupData[{
Cell[2738, 87, 2425, 51, 360, "Input",ExpressionUUID->"27ce4f4b-d8bb-462d-8cc1-221333f8328a"],
Cell[5166, 140, 761, 20, 57, "Output",ExpressionUUID->"c9cd9b0f-bb77-4a6b-8cbc-09236cca7b38"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[5976, 166, 167, 3, 41, "Subsubsection",ExpressionUUID->"bed51261-985b-4067-bf67-e983bcbd1518"],
Cell[CellGroupData[{
Cell[6168, 173, 1015, 24, 228, "Input",ExpressionUUID->"c603d323-5633-4ec5-9e6f-f07d49a08bac"],
Cell[7186, 199, 910, 25, 70, "Output",ExpressionUUID->"cd9b82e9-2c05-400e-8428-a254c4397eb8"]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[8145, 230, 174, 3, 34, "Subsubsection",ExpressionUUID->"c7f60019-cba3-4fd1-a8c7-d4cfe96645c3"],
Cell[CellGroupData[{
Cell[8344, 237, 962, 24, 140, "Input",ExpressionUUID->"ff4abe39-f9a0-4c72-b5f0-1e4cecaaa546"],
Cell[9309, 263, 625, 18, 70, "Output",ExpressionUUID->"68240a31-c500-4ccc-b5ca-94cc6a5894a7"]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[9983, 287, 162, 3, 36, "Subsection",ExpressionUUID->"ea61d37a-3fd4-4a1a-8918-8282d9e56eee"],
Cell[10148, 292, 158, 3, 33, "Text",ExpressionUUID->"88741a4d-f78d-4798-95d2-1af57d4d42be"],
Cell[CellGroupData[{
Cell[10331, 299, 935, 24, 90, "Input",ExpressionUUID->"4c3cae32-8d9f-4405-bc20-768b747b5615"],
Cell[11269, 325, 1076, 24, 57, "Output",ExpressionUUID->"f7764a79-760e-4eaf-9d14-59e504d65a21"],
Cell[12348, 351, 824, 17, 58, "Output",ExpressionUUID->"b443ccae-6af1-49ab-b219-028e14780631"]
}, Open  ]],
Cell[13187, 371, 209, 3, 33, "Text",ExpressionUUID->"009d1802-d61c-4725-bea0-4f04ef21a0fa"],
Cell[CellGroupData[{
Cell[13421, 378, 525, 12, 90, "Input",ExpressionUUID->"bc1d5add-0e4b-4ffd-85df-867cb4c59e65"],
Cell[13949, 392, 776, 20, 58, "Output",ExpressionUUID->"5ddbff22-4c8c-461a-bbe2-392af68ecd57"],
Cell[14728, 414, 453, 8, 53, "Output",ExpressionUUID->"2b020099-8d88-477a-88b9-b5f77ecb585a"]
}, Open  ]],
Cell[15196, 425, 168, 3, 33, "Text",ExpressionUUID->"aff69f27-c245-4ef4-a381-7c050a6c7051"],
Cell[CellGroupData[{
Cell[15389, 432, 246, 5, 29, "Input",ExpressionUUID->"fd59c4cc-db8d-4189-a4f7-6d0f4361d7d9"],
Cell[15638, 439, 576, 16, 58, "Output",ExpressionUUID->"aa9b2292-d177-4cc8-8a5d-a13eb5fc000d"]
}, Open  ]],
Cell[16229, 458, 215, 4, 33, "Text",ExpressionUUID->"ccc2b872-c740-4efc-ba75-95c308286ec1"],
Cell[CellGroupData[{
Cell[16469, 466, 461, 12, 72, "Input",ExpressionUUID->"f63d9340-a163-4b06-806d-d70b035e1b94"],
Cell[16933, 480, 567, 16, 100, "Output",ExpressionUUID->"9767ab1a-0049-42ae-9f8e-ad322a5f882e"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[17549, 502, 209, 4, 50, "Subsection",ExpressionUUID->"1a86bdb0-d846-4061-976e-29c9f0e6efb2"],
Cell[17761, 508, 158, 3, 33, "Text",ExpressionUUID->"67816b1d-6c76-462c-9de4-40f42f470f41"],
Cell[CellGroupData[{
Cell[17944, 515, 1214, 29, 90, "Input",ExpressionUUID->"5dd7ad58-395c-493f-9dbc-81bfbb18c348"],
Cell[19161, 546, 1642, 39, 61, "Output",ExpressionUUID->"9e10b7fa-14f8-43e3-bfef-12125df5242f"],
Cell[20806, 587, 940, 17, 51, "Output",ExpressionUUID->"48dbfab1-858b-4a51-a6f8-9ce24cb52701"]
}, Open  ]],
Cell[21761, 607, 209, 3, 33, "Text",ExpressionUUID->"9995f71f-1a71-43b0-8c62-d4adcd24561d"],
Cell[CellGroupData[{
Cell[21995, 614, 608, 14, 112, "Input",ExpressionUUID->"32c8278a-67fa-41d9-8a14-0bf9b46ad126"],
Cell[22606, 630, 397, 6, 33, "Output",ExpressionUUID->"8d4a0082-5ff4-430a-aaa4-463657ba541a"],
Cell[23006, 638, 395, 6, 33, "Output",ExpressionUUID->"b486762a-9e52-41dc-ba66-aaeab527b5de"]
}, Open  ]],
Cell[23416, 647, 163, 3, 33, "Text",ExpressionUUID->"e34773bd-4aea-464a-b939-b102a0e6e444"],
Cell[CellGroupData[{
Cell[23604, 654, 445, 11, 72, "Input",ExpressionUUID->"9b6c60d0-3f87-43cc-bfe4-bd10013a9a91"],
Cell[24052, 667, 443, 13, 60, "Output",ExpressionUUID->"5b974c98-b98c-4fb4-91b8-fc56864f84bd"]
}, Open  ]]
}, Open  ]]
}
]
*)

