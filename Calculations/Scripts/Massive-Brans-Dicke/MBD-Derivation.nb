(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      5567,        176]
NotebookOptionsPosition[      4948,        157]
NotebookOutlinePosition[      5300,        173]
CellTagsIndexPosition[      5257,        170]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"V", " ", "=", " ", 
   RowBox[{
    RowBox[{"-", " ", 
     FractionBox[
      RowBox[{"M", " ", "\[Mu]"}], "r"]}], "\[ScriptCapitalG]"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.757076972613985*^9, 3.757077000734951*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"2aa91910-afe8-4cd2-a15c-65f12800407a"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"templeft", " ", "=", 
    RowBox[{"Series", "[", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          SubscriptBox["\[PartialD]", "r"], "V"}], ")"}], " ", "/.", 
        RowBox[{"\[ScriptCapitalG]", "\[Rule]", " ", 
         RowBox[{"\[ScriptCapitalG]0", "+", 
          RowBox[{"\[Epsilon]", " ", "\[ScriptCapitalG]1"}]}]}]}], "/.", 
       RowBox[{"r", "\[Rule]", " ", 
        RowBox[{"r0", "+", 
         RowBox[{"\[Epsilon]", " ", "r1"}]}]}]}], ",", 
      RowBox[{"{", 
       RowBox[{"\[Epsilon]", ",", "0", ",", "1"}], "}"}]}], "]"}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"tempright", " ", "=", 
   RowBox[{"Series", "[", " ", 
    RowBox[{
     RowBox[{
      RowBox[{"\[Mu]", " ", "r", " ", 
       SuperscriptBox["\[Omega]", "2"]}], "/.", 
      RowBox[{"r", "\[Rule]", " ", 
       RowBox[{"r0", "+", 
        RowBox[{"\[Epsilon]", " ", "r1"}]}]}]}], ",", 
     RowBox[{"{", 
      RowBox[{"\[Epsilon]", ",", "0", ",", "1"}], "}"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"r0eq", " ", "=", " ", 
   RowBox[{"r0", "/.", 
    RowBox[{
     RowBox[{"Solve", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"Coefficient", "[", 
         RowBox[{"templeft", ",", "\[Epsilon]", ",", "0"}], "]"}], "\[Equal]",
         " ", 
        RowBox[{"Coefficient", "[", 
         RowBox[{"tempright", ",", "\[Epsilon]", ",", "0"}], "]"}]}], ",", 
       "r0"}], "]"}], "[", 
     RowBox[{"[", "1", "]"}], "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"r0cubed", " ", "=", 
   RowBox[{
    SuperscriptBox["r0", "3"], "/.", 
    RowBox[{"r0", "\[Rule]", " ", "r0eq"}]}]}], ";"}], "\[IndentingNewLine]", 

 RowBox[{
  RowBox[{
   RowBox[{"r1eq", " ", "=", " ", 
    RowBox[{
     RowBox[{"r1", "/.", 
      RowBox[{
       RowBox[{"Solve", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"Coefficient", "[", 
           RowBox[{"templeft", ",", "\[Epsilon]", ",", "1"}], "]"}], 
          "\[Equal]", " ", 
          RowBox[{"Coefficient", "[", 
           RowBox[{"tempright", ",", "\[Epsilon]", ",", "1"}], "]"}]}], ",", 
         "r1"}], "]"}], "[", 
       RowBox[{"[", "1", "]"}], "]"}]}], "/.", 
     RowBox[{
      SuperscriptBox["r0", "3"], "\[Rule]", " ", "r0cubed"}]}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{"Framed", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"HoldForm", "[", 
     SuperscriptBox[
      SubscriptBox["r", "0"], "3"], "]"}], "\[Equal]", " ", "r0cubed"}], ",", 
   
   RowBox[{"Background", "\[Rule]", "LightBlue"}]}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"Framed", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"HoldForm", "[", 
     SubscriptBox["r", "1"], "]"}], "\[Equal]", " ", "r1eq"}], ",", 
   RowBox[{"Background", "\[Rule]", "Yellow"}]}], 
  "]"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.757077011475584*^9, 3.757077492677886*^9}, {
  3.757077526823579*^9, 3.757077575999744*^9}},
 CellLabel->
  "In[150]:=",ExpressionUUID->"81ea2a60-03df-4a2a-8414-739394a993e0"],

Cell[BoxData[
 FrameBox[
  RowBox[{
   TagBox[
    SubsuperscriptBox["r", "0", "3"],
    HoldForm], "\[Equal]", 
   FractionBox[
    RowBox[{"M", " ", "\[ScriptCapitalG]0"}], 
    SuperscriptBox["\[Omega]", "2"]]}],
  Background->RGBColor[0.87, 0.94, 1],
  StripOnInput->False]], "Output",
 CellChangeTimes->{{3.757077540651845*^9, 3.757077576410063*^9}},
 CellLabel->
  "Out[155]=",ExpressionUUID->"625ee27c-5b61-4ad6-92ac-344a9982e724"],

Cell[BoxData[
 FrameBox[
  RowBox[{
   TagBox[
    SubscriptBox["r", "1"],
    HoldForm], "\[Equal]", 
   FractionBox[
    RowBox[{"r0", " ", "\[ScriptCapitalG]1"}], 
    RowBox[{"3", " ", "\[ScriptCapitalG]0"}]]}],
  Background->RGBColor[1, 1, 0],
  StripOnInput->False]], "Output",
 CellChangeTimes->{{3.757077540651845*^9, 3.757077576429008*^9}},
 CellLabel->
  "Out[156]=",ExpressionUUID->"8a975bb2-9ea3-453d-be51-b353e010ed25"]
}, Open  ]]
},
WindowSize->{808, 659},
WindowMargins->{{4, Automatic}, {Automatic, 4}},
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 339, 9, 47, "Input",ExpressionUUID->"2aa91910-afe8-4cd2-a15c-65f12800407a"],
Cell[CellGroupData[{
Cell[922, 33, 3134, 91, 245, "Input",ExpressionUUID->"81ea2a60-03df-4a2a-8414-739394a993e0"],
Cell[4059, 126, 438, 13, 62, "Output",ExpressionUUID->"625ee27c-5b61-4ad6-92ac-344a9982e724"],
Cell[4500, 141, 432, 13, 95, "Output",ExpressionUUID->"8a975bb2-9ea3-453d-be51-b353e010ed25"]
}, Open  ]]
}
]
*)

